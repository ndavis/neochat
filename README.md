# Neochat

Neochat is a client for Matrix, the decentralized communication protocol for instant
messaging. It is a fork of Spectral, using KDE frameworks (Kirigami and KI18n).

## Contact

You can reach the maintainer at #plasma-mobile:matrix.org, if you are already on Matrix.

## Acknowledgement

This program utilizes [libQuotient](https://github.com/quotient-im/libQuotient/) library and some C++ models from [Quaternion](https://github.com/quotient-im/Quaternion/).

This program is a fork of [Spectral](https://gitlab.com/spectral-im/spectral/).

This program includes a copy of [WinToast](https://github.com/mohabouje/WinToast/) to display notifications.

## License

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is licensed under GNU General Public License, Version 3. 

